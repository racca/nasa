//
//  MeteorList.swift
//  nasa
//
//  Created by Thiago Racca IE on 17/02/2019.
//  Copyright © 2019 Thiago Racca IE. All rights reserved.
//

import Foundation

class MeteorListModel {
    
    var meteors : [Meteor] = []
    
    func loadMeteors(_ completion: @escaping (_ result: Bool) -> Void){
        MeteorPersistence().getMeteors(){
            result in
            
            self.meteors = result
            completion(true)
        
        }
    }
}
