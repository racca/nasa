//
//  MeteorListTableViewController.swift
//  nasa
//
//  Created by Thiago Racca IE on 17/02/2019.
//  Copyright © 2019 Thiago Racca IE. All rights reserved.
//

import UIKit

class MeteorListTableViewController: UITableViewController {

    var model = MeteorListModel()
    
    var selectedMeteor : Meteor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Meteors"

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false
        navigationItem.hidesBackButton = true
        
        self.model.loadMeteors() {
            result in
            if result {
                self.tableView.reloadData()
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.model.meteors.count
    }

  
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "meteorCell", for: indexPath) as? MeteorListTableViewCell {
        
            let name = self.model.meteors[indexPath.row].name.replacingOccurrences(of: "\\", with: "")
            cell.meteorName.text = " Name: \(name)"
            cell.meteorMass.text = " Mass: \(self.model.meteors[indexPath.row].mass ?? "")"
            
            let dateFor: DateFormatter = DateFormatter()
            dateFor.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            
            if let yearDate = self.model.meteors[indexPath.row].year,
                let date = dateFor.date(from: yearDate){
                
                
                let calendar = Calendar.current
                let components = calendar.dateComponents([.year], from: date)
            
                let year =  components.year
                cell.meteorDate.text = " Year: \(year ?? 0)"
                
      
            }
            
            
            
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.selectedMeteor = self.model.meteors[indexPath.row]
        
        self.performSegue(withIdentifier: "showMap", sender: self)
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMap",
           let controller = segue.destination as? MapViewController {
            controller.model.meteor = self.selectedMeteor
        }
    }
    
  
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
