//
//  MeteorListTableViewCell.swift
//  nasa
//
//  Created by Thiago Racca IE on 17/02/2019.
//  Copyright © 2019 Thiago Racca IE. All rights reserved.
//

import UIKit

class MeteorListTableViewCell: UITableViewCell {
    @IBOutlet weak var meteorName: UILabel!
    @IBOutlet weak var meteorMass: UILabel!
    @IBOutlet weak var meteorDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
