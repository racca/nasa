//
//  ViewModelDelegate.swift
//  nasa
//
//  Created by Thiago Racca IE on 17/02/2019.
//  Copyright © 2019 Thiago Racca IE. All rights reserved.
//

import Foundation

protocol MainViewModelDelate {
    
    func loadingFinished()
}
