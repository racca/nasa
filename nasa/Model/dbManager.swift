//
//  dbManager.swift
//  nasa
//
//  Created by Thiago Racca IE on 13/02/2019.
//  Copyright © 2019 Thiago Racca IE. All rights reserved.
//

import Foundation
import GRDB

//controls the copy of the database

class dbManager {
    
    static var shared : dbManager = {
        let instance = dbManager()
        return instance
    }()
    
    // Simple database connection
    var dbQueue : DatabaseQueue?
    
    // Enhanced multithreading based on SQLite's WAL mode
    var dbPool : DatabasePool?
    
    
    init(){
        if !self.checkIfDatabaseAlreadyExists() {
            self.copyDatabase()
        }
        
        let dbLibraryUrl = self.getDbLibraryUrl()
        print("database path \(dbLibraryUrl.path)")
        
        do {
            try self.dbQueue = DatabaseQueue(path: self.getDbPath())
            try self.dbPool = DatabasePool(path: self.getDbPath())
            print("database connection stabilished sucessfully")
        }
        catch let error {
            print("Error trying to stabilish database connection \(error)")
        }
    }
    
    
    @discardableResult func copyDatabase() -> Bool {
        
        guard let dbBundleUrl = self.getDbFolderOnBundle() else {return false}
         let dbLibraryUrl = self.getDbLibraryUrl()
        let fileManagerIs = FileManager.default
        do {
            if !self.checkIfDatabaseAlreadyExists() {
                try fileManagerIs.copyItem(atPath: dbBundleUrl.path, toPath:dbLibraryUrl.path)
                print("created succesfully")
                return true
            }
            else{
                print("database already exists")
            }
            
            
        } catch let error  {
            print("\nError\n\(error)")
        }
        
        return false
    }
    
    func checkIfDatabaseAlreadyExists() ->Bool {
        let databaseUrl = self.getDbFolder().appendingPathComponent("db.sqlite")
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: databaseUrl.path) {
            return true
        } else {
            return false
        }
    }
    
    func getDbPath() -> String{
        return FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)[0].appendingPathComponent("db.sqlite").path
        
    }
    
    func getDbLibraryUrl() -> URL{
        return FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)[0].appendingPathComponent("db.sqlite")
        
    }
    
    func getDbFolder() -> URL{
            return FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)[0]
    
    }
    
    func getDbFolderOnBundle() -> URL? {
        return Bundle.main.url(forResource: "db", withExtension: "sqlite")
    }
}
