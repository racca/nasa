//
//  Meteor.swift
//  nasa
//
//  Created by Thiago Racca IE on 13/02/2019.
//  Copyright © 2019 Thiago Racca IE. All rights reserved.
//

import Foundation

struct Meteor : Codable {
    var id : String
    var mass : String?
    var name : String
    var fell : String?
    var recclass : String?
    var nametype : String?
    var reclat : String?
    var reclong : String?
    var year : String?
    var geolocation : MeteorGeolocation?
}


