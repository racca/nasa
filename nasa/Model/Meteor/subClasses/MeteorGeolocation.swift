//
//  MeteorGeolocation.swift
//  nasa
//
//  Created by Thiago Racca IE on 13/02/2019.
//  Copyright © 2019 Thiago Racca IE. All rights reserved.
//

import Foundation

struct MeteorGeolocation : Codable {
    var type : String
    var coordinates : [Double]
}



