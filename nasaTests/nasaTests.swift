//
//  nasaTests.swift
//  nasaTests
//
//  Created by Thiago Racca IE on 12/02/2019.
//  Copyright © 2019 Thiago Racca IE. All rights reserved.
//

import XCTest

@testable import nasa

class nasaTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

 
    func testDatabaseAlreadyExists() {
        XCTAssert(dbManager().checkIfDatabaseAlreadyExists())
    }
    

    
    
    func testLoadData() {
        
        // Create an expectation for a background download task.
        let expectation = XCTestExpectation(description: "load database data")
        
        MeteorPersistence().getMeteors() {
            meteors in
            
            if (meteors.count > 0) {
                expectation.fulfill()
            }
        }
        
        
        // Wait until the expectation is fulfilled, with a timeout of 10 seconds.
        wait(for: [expectation], timeout: 10.0)
    }
    
    

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}


